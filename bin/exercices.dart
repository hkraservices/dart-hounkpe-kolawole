import 'package:exercices/etape1.dart';
import 'package:exercices/etape2.dart';
import 'package:exercices/etape3.dart';
import 'package:exercices/etape4.dart';

void main(List<String> arguments) {
  prepare(consttexte);

  map([
    'but',
    'est',
    'trouver',
    'mot',
    'lettres',
    'plus',
    'avec',
    'plus',
    'occurence',
    'dans',
    'ensemble',
    'textes',
    'utilisant',
    'principe',
    'légèrement',
    'similaire',
    'algorithme',
    'mapreduce'
  ]);

  reduce([
    '{"mot1": 1}',
    '{"mot2": 2}',
    '{"mot1": 3, "mot3": 4}',
  ]);
}
